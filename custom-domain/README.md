# Custom Domain

Buat domain di Cloudflare. Masukkan IP Address sesuai dengan Elastic IP pada server AWS.
<img src="ss/1.png" align="center">

Edit file <code>/etc/nginx/leon/leon.conf</code>. Ubah <code>server_name</code>, dari IP ke nama domain.
<img src="ss/2.png" align="center">

Simpan dan reload nginx.
```
sudo systemctl reload nginx
```

Enable <code>ufw</code> untuk mengijinkan port 80 dan 433.

<img src="ss/3.png" align="center">

## Buka Browser
Masukkan nama domain.
<img src="ss/4.png" align="center">