# Server For Application

**Note** : Karena tanpa Elastic IP server tidak mendapatkan koneksi internet, maka tambahkan dulu elastic ip. Bisa dilihat <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/create-and-setup-aws-server#elastic-ip">disini</a>

Masuk melalui server public.

<img src="ss/0.png" align="center">


Update & Upgrade Ubuntu
```
sudo apt update && sudo apt upgrade
```
<img src="ss/1.png" align="center">

Install **Node.JS v.10.x**
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

```

Clone app **Node.JS** milih mas sugeng.
```
git clone https://github.com/sgnd/dumbplay.git

```

Agar Node App dapat berjalan di background, saya membutuhkan <code>pm2</code>. Install secara global melalui <code>npm</code>.
```
sudo npm install -g pm2
```

Masuk ke directory <code>dumbplay/frontend</code>
```
cd dumbplay/frontend
```

Buat file <code>ecosystem.config.js</code> lalu simpan konfigurasinya.

<img src="ss/2.png" align="center">


Lalu jalankan <code>pm2</code>
```
pm2 start ecosystem.config.js
```
<img src="ss/3.png" align="center">

Agar <code>pm2</code> dapat berjalan secara otomatis setelah server di reboot.
```
pm2 startup
```

### Ending
Hapus lagi Elastic IP yang sudah ditambahkan untuk mendapatkan koneksi internet. Buka link <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/create-and-setup-aws-server#kembali-ke-ec2-aws">berikut</a>.