# SSL Configuration

Setelah custom domain **Cloudflare** sudah didaftarkan, sekarang saya akan menggunakan SSL **Let'sEncrypt** dan tidak menggunakan SSL Cloudflare.

Referensi pemasangan SSL LetsEncrypt bisa dicek <a href="https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx">disini</a>. Pemasangannya sangat mudah, hanya membutuhkan paket yang bernama **Certbot**. Paket ini mengautomasi pemasangan SSL dan me-manage server kita agar siap menjalankan SSL LetsEncrypt.

Pastikan snap sudah diversi terbaru.
```
sudo snap install core; sudo snap refresh core
```

Install **certbot** melalui snap
```
sudo snap install --classic certbot
```

Link certbot ke direktori <code>/usr/bin</code> agar paket bisa dijalankan secara global melalui terminal.
```
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```

Install SSL LetsEncrypt
```
sudo certbot --nginx -d leon.instructype.com
```

Gambar pemasangan SSL secara keseluruhan dan informasi bahwa SSL sudah berhasil dipasang
<img src="ss/1.png" align="center">

Konfigurasi nginx sudah disetting secara otomatis oleh Certbot
<img src="ss/2.png" align="center">

## Cek Ke Browser
#### Sebelum
<img src="ss/sebelum.png" align="center">
Tidak ada <code>https://</code> pada link ini.

#### Sesudah
<img src="ss/3.png" align="center">
<code>https://</code> Tersedia pada link ini.

#### Info SSL
<img src="ss/4.png" align="center">

Cek link berikut.
https://leon.instructype.com