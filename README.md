# Praktek Penggunaan AWS

### Masih week 1 bersama <a href="https://dumbways.id">DumbWays.id</a>

## Daftar Isi
- <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/create-and-setup-aws-server">Create & Setup Server</a>
- <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/aws-app-private">Server for Application</a>
- <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/aws-reverse-proxy">Reverse Proxy</a>
- <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/custom-domain">Custom Domain</a>
- <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/ssl-configuration">SSL Configuration</a>

## Special thanks 
- Bootcamp Dumbways.id
- Temen" bootcamp
- Dan mentor" tercinta ❤️ yang selalu membantu jika ditanya

## References
- https://www.linode.com/docs/guides/install-lets-encrypt-to-create-ssl-certificates/ 
- https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx