# Server for Reverse Proxy

Dari dalam server sebelumnya <a href="https://gitlab.com/ledleledle/praktek-aws/-/tree/master/aws-app-private">Private Server</a>, keluar <code>Ctrl+D</code> dan saya sudah kembali ke server public.

Lakukan update & upgrade pada OS seperti pada private server sebelumnya.
<img src="ss/1.png" align="center">

Install <code>nginx</code>.
```
sudo apt install nginx
```

Masuk ke directory <code>nginx</code> dan saya membuat folder bernama <code>leon</code>.
```
cd /etc/nginx
sudo mkdir leon
```

Lalu buat file bernama <code>leon.conf</code> didalam folder <code>leon</code>. Lalu isi konfigurasi untuk reverse proxy. Simpan konfigurasi
```
sudo nano leon/leon.conf
```
<img src="ss/3.png" align="center">

Buka file <code>nginx.conf</code> dan tambahkan baris berikut. Untuk mendefinisikan folder <code>leon</code>.
<img src="ss/2.png" align="center">

Restart <code>nginx</code>.
```
sudo systemctl restart nginx
```

### Buka browser
<img src="ss/4.png" align="center">