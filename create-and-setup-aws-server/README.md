# Create & Setup AWS Server

## Launch Instance
Pertama masuk ke EC2 Management Console AWS, lalu pilih **Launch Instance**, disini saya menggunakan **Ubuntu Server 18.04**.
<img src="ss/1.png" align="center">

Pilih spesifikasi yang dibutuhkan
<img src="ss/2.png" align="center">

Konfigurasi Instance, pilih **Auto assign IP** menjadi **Disable**
<img src="ss/3.png" align="center">

Tentukan storage yang dibutuhkan (disini saya membuat 10GB).
<img src="ss/4.png" align="center">

Skip tags, lalu atur security group (untuk reverse proxy buka port 22, 80 & 443 saja)
<img src="ss/5.png" align="center">

Pada saat tahap review, pastikan semua setting yang dibuat sudah sesuai keinginan. Jika sudah merasa benar klik **Launch**. Jika belum membuat keypair, pilih **Create New Keypair** lalu download keypair. Keypair jangan sampai hilang, sekalinya hilang kita tidak bisa login ke instance lagi.
<img src="ss/6.png" align="center">

Ulangi tahap <a href="#launch-instance">diatas</a> dan beri nama pada instance untuk memudahkan pengenalannya.
<img src="ss/7.png" align="center">

#### Review Instance
| Item             | Server Public        | Server Private        |
|------------------|----------------------|-----------------------|
| Security Group   | 22,80,443            | 22,80,443 (Sementara) |
| Storage          | 10GB                 | 10GB                  |
| OS               | Ubuntu Server 18.04  | Ubuntu Server 18.04   |
| Private IP       | 172.31.48.212        | 172.31.28.20          |


## Elastic IP
Pergi ke menu **Elastic IP**, pilih menu **Allocate Elastic IP address**, lalu **Allocate**
<img src="ss/8.png" align="center">

Centang lalu pilih menu **Associate Elastic IP address**. 
<img src="ss/9.png" align="center">

Saya arahkan ke server public terlebih dahulu. lalu klik **Associate**
<img src="ss/10.png" align="center">

Ulangi tahap <a href="#elastic-ip">diatas</a> arahkan yang satunya ke server private.
<img src="ss/11.png" align="center">

## SSH
**Note** : Sebelum masuk ke server melalui SSH pastikan sudah memberikan permission yang benar pada Keypair yang sudah didownload sebelumnya
```
chmod 400 nama_keypair
```

Masuk ke kedua server
```
ssh ubuntu@elastic_IP -i key_pair.pem
```
<img src="ss/12.png" align="center">

*Karena saya sudah membuat user di server private jadi saya hanya kan menampilkan tahap pembuatan user pada server private*. Untuk membuat user baru masukkan perintah <code>adduser</code> masukkan nama user&password lalu berikan akses sudo.
<img src="ss/13.png" align="center">

Edit file <code>/etc/ssh/sshd_config</code> cari kata <code>PasswordAuthentication</code> rubah dari **no** menjadi **yes**. Simpan file dan restart <code>sshd.service</code>
```
sudo systemctl restart sshd.service
```
<img src="ss/14.png" align="center">

Coba login ke server private melalui SSH tanpa keypair. Jika sudah dipastikan bisa, keluar dari SSH dulu.
<img src="ss/15.png" align="center">

**Note** : Untuk membuatnya benar-benar private saya harus menghapus elastic IP dari server private dan mengganti security group menjadi **All Traffic**.

## Kembali ke EC2 AWS
Masuk ke menu **Elastic IP**, pilih IP yang digunakan pada private server. Lalu **Disassociate**.
<img src="ss/16.1.png" align="center">

Tidak itu saja, **Release Elastic IP** untuk menghapus IP yang sudah tidak digunakan.
<img src="ss/16.2.png" align="center">

Ubah security group milik private server.
<img src="ss/17.png" align="center">

Remove security group milik public server dan tambahkan security group untuk private server.
<img src="ss/18.1.png" align="center">

Isi dari security group private server
<img src="ss/18.2.png" align="center">

Masuk ke private server melalui public server
```
ssh leon@elastic_ip

#lalu

ssh leonprivate@private_ip
```
<img src="ss/19.png" align="center">

#### Review Keseluruhan Server
| Item             | Server Public        | Server Private        |
|------------------|----------------------|-----------------------|
| Security Group   | 22,80,443            | All Traffic           |
| Private IP       | 172.31.48.212        | 172.31.28.20          |
| Elastic IP       | Yes                  | No                    |

## Bonus
Buat snapshot untuk backup instance atau volume.
<img src="ss/snapshot.png" align="center">